<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->boolean('guid_permalink')->nullable();
            $table->string('source')->index();
            $table->string('title');
            $table->string('slug')->index();
            $table->string('link');
            $table->text('description');
            $table->text('content')->nullable();
            $table->string('author');
            $table->integer('category_id')->index();
            $table->string('comments');
            $table->timestamp('pubDate');
            $table->string('enclosure_url')->nullable();
            $table->bigInteger('enclosure_length')->nullable();
            $table->string('enclosure_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}

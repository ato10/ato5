#!/bin/bash
composer require arrilot/laravel-widgets
composer require creativeorange/gravatar
composer require guzzlehttp/guzzle
composer require intervention/image
composer require intervention/imagecache
composer require messerli90/igdb
composer require php-tmdb/laravel
composer require predis/predis
composer require rap2hpoutre/laravel-log-viewer
composer require simplepie/simplepie
composer require spatie/flysystem-dropbox
composer require spatie/laravel-backup
composer require spatie/laravel-html
composer require watson/rememberable
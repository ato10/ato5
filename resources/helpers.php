<?php

if (!function_exists('human_filesize')) {
    function human_filesize($bytes, $decimals = 2)
    {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
    }
}

if (!function_exists('get_filter')) {
    function get_filter($k, $v)
    {
        $request = request();
        if (empty($v)) {
            return $request->fullUrlWithQuery([
                'page' => null,
            ]);
        } else {
            return $request->fullUrlWithQuery([
                'page' => null,
                $k => $v,
            ]);
        }
    }
}

if (!function_exists('print_filter')) {
    function print_filter($k)
    {
        $request = request();
        switch ($k) {
            case 'a':
                if (empty($request->get('a', ''))) {
                    return '';
                } else {
                    return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'a' => null]) . '"><span class="badge badge-danger"><i class="icon icon-close" aria-hidden="true"></i> ' . $request->get('a') . '</span></a>';
                }
            break;
            case 'q':
                if (empty($request->get('q', ''))) {
                    return '';
                } else {
                    return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'q' => null]) . '"><span class="badge badge-primary"><i class="icon icon-close" aria-hidden="true"></i> ' . $request->get('q') . '</span></a>';
                }
            break;
            case 'c':
                if (empty($request->get('c', ''))) {
                    return '';
                } else {
                    $cat = \App\Models\FeedCategory::findOrFail($request->get('c'));
                    return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'c' => null]) . '"><span class="badge badge-success"><i class="icon icon-close" aria-hidden="true"></i> ' . $cat->name . '</span></a>';
                }
            break;
        }
    }
}
<div class="social-box film">
    <i class="icon {!! $icon !!}"></i>
    <br>
    <h3>{{ $title }}</h3>
    <br>
    <div>
        <a class="btn btn-primary btn-lg" href="{!! $link !!}">View all {{ $config['title'] }}</a>
    </div>
    <br>
</div>
<!--/social-box-->
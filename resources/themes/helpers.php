<?php

if (!function_exists('theme_path')) {
    function theme_path($filename = null)
    {
        return resource_path('themes/' . config('site.theme') . '/' . $filename);
    }
}

if (!function_exists('theme_url')) {
    function theme_url($url)
    {
        return url(config('site.theme') . '/' . $url);
    }
}

if (!function_exists('classActivePath')) {
    function classActivePath($path)
    {
        return Request::is($path) ? ' active' : '';
    }
}

if (!function_exists('classActiveSegment')) {
    function classActiveSegment($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? ' active' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return ' active';
        }
        return '';
    }
}
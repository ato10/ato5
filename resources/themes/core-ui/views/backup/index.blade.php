@extends('layouts.app')

@section('title', 'Backups')

@section('breadcrumb')
@parent
<li class="breadcrumb-item">Backups</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-drawer"></i> Backups</strong></div>
    <div class="card-block">
        
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-responsive table-hover table-striped">
                    <tr>
                        <th>name</th>
                        <th>modified</th>
                        <th>size</th>
                    </tr>
                    @foreach ($backups as $backup)
                    <tr>
                        <td>{{ $backup['basename'] }}</td>
                        <td>{{ $backup['modified'] }}</td>
                        <td>{{ human_filesize($backup['size'], 2) }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        
    </div>
</div>
@endsection

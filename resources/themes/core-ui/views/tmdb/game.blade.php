@extends('layouts.app')

@section('title', 'Tmdb')

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('feeds.index') !!}">Feeds</a></li>
<li class="breadcrumb-item">Tmdb</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
		<strong><i class="icon-feed"></i> {{ $feed->title }}</strong>
	</div>
    <div class="card-block">

        <div class="medias">
            @foreach ($games as $game)
            <div class="row">
                <div class="col-md-4">
                    @if (property_exists($game, 'screenshots'))
                    <img class="img-responsive img-thumbnail" src="{!! str_replace_first('t_thumb/', 't_screenshot_huge/', $game->screenshots[0]->url) !!}">
                    @endif
                </div>
                <div class="col-md-8">
                    <h2>{{ $game->name }}</h2>
                    @if (property_exists($game, 'summary'))
                    {{ $game->summary }}
                    @endif
                    <br>
                    <br>
                    <a class="btn btn-warning" target="_blank" href="{!! $game->url !!}">
                        Internet Game Database (IGDB)
                    </a>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
@endsection

@section('css')
@parent
@endsection
@extends('layouts.app')

@section('title', 'Tmdb')

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('feeds.index') !!}">Feeds</a></li>
<li class="breadcrumb-item">Tmdb</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
		<strong><i class="icon-feed"></i> {{ $feed->title }}</strong>
	</div>
    <div class="card-block">

		@empty($tmdb['results'])
			@include('tmdb.partials.search')
		@else
			<div class="medias">
				@foreach ($tmdb['results'] as $movie)
				<div class="row">
					<div class="col-md-4">
						@if ($movie['poster_path'] !== null)
						<img class="img-responsive img-thumbnail" src="https://image.tmdb.org/t/p/w300{{ $movie['poster_path'] }}">
						@endif
					</div>
					<div class="col-md-8">
						<h2>{{ $movie['title'] }}</h2>
						@include('tmdb.partials.stars') {{ $movie['overview'] }}
						<br>
						<br>
						<a class="btn btn-warning" target="_blank" href="https://www.themoviedb.org/movie/{{ $movie['id'] }}">
							The Movie Database (TMDB)
						</a>
						<a class="btn btn-primary" href="{!! $feed->enclosure_url !!}" target="_blank">
							<i class="icon icon-cloud-download" aria-hidden="true"></i> Download
						</a>
					</div>
				</div>
				@endforeach
			</div>
		@endempty

    </div>
</div>
@endsection

@section('css')
@parent
@include('tmdb.partials.stars_css')
@endsection
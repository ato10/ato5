<style>
	.star-ratings-css {
		cursor: pointer;
		color: #c5c5c5;
		font-size: 18px;
		width: 101px;
		position: relative;
		padding: 0;
		text-shadow: 0px 1px 0 #a2a2a2;
        overflow: hidden;
		white-space: nowrap;
	}

	.star-ratings-css-top {
		color: #e7711b;
		padding: 0;
		position: absolute;
		z-index: 1;
		display: block;
		top: 0;
		left: 0;
		overflow: hidden;
		white-space: nowrap;
	}

    .star-ratings-css-bottom {
        padding: 0;
        display: block;
        z-index: 0;
		white-space: nowrap;
    }

	.star-ratings-css div {
		word-wrap: normal;
	}
</style>
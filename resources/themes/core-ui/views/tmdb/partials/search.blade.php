<div class="row">
    <div class="col-sm-12">
        <form target="_blank" action="https://www.themoviedb.org/search" method="get">
            <div class="form-group">
                <label for="query">The Movie Database</label>
                <input class="form-control" id="query" name="query" value="{!! $search !!}" type="text">
            </div>
            <input class="btn btn-default" type="submit" value="cerca" alt="Submit">
        </form>
    </div>
</div>
<div class="star-ratings-css" data-toggle="tooltip" data-placement="top" title="{!! $movie['vote_average']*10 !!}% user score">
    <div class="star-ratings-css-top" style="width: {!! $movie['vote_average']*10 !!}%">
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
    </div>
    <div class="star-ratings-css-bottom">
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
    </div>
</div>
{{ $movie['vote_count'] }} votes
<br><br>
@extends('layouts.app')

@section('breadcrumb')
@parent
<li class="breadcrumb-item">Channels</li>
@endsection

@section('breadcrumb-menu')
<li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="{!! route('channels.create') !!}">
            <i class="icon-plus"></i> &nbsp;Create channels</a>
    </div>
</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-feed"></i> Channels</strong></div>
    <div class="card-block">
                    
        <div class="table-responsive">
            <table class="table table-hover table-feeds">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="tbody">
                @foreach ($channels as $channel)
                    <tr class="tr">
                        <td>
                            {{ $channel->name }}<br>
                            <small>{{ $channel->url }}</small>
                        </td>
                        <td>{{ $channel->type }}</td>
                        <td>{{ $channel->category }}</td>
                        <td>{!! $channel->active ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-secondary">disabled</span>' !!}</td>
                        <th>
                            <a class="btn btn-primary icon-settings" href="{!! route('channels.edit', $channel) !!}"></a>
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

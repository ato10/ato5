@extends('layouts.app')

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('channels.index') !!}">Channels</a></li>
<li class="breadcrumb-item">Create channel</li>
@endsection

@section('content')

{{ html()->form('POST', route('channels.store'))->open() }}
    <div class="card">
        <div class="card-header"><strong><i class="icon-feed"></i> Channels</strong></div>
        <div class="card-block">

            <div class="form-group">
                <label for="name">Name</label>
                {{ html()->text('name')->class('form-control')->placeholder('Name') }}
            </div>

            <div class="form-group">
                <label for="url">URL</label>
                {{ html()->text('url')->class('form-control')->placeholder('URL') }}
            </div>

            <div class="form-group">
                <label for="type">Type</label>
                {{ html()->select('type')->class('form-control')->options(['rss20' => "rss20", 'rss20_2' => 'rss20_2']) }}
            </div>

            <div class="form-group">
                <label for="category">Category</label>
                {{ html()->text('category')->class('form-control')->placeholder('Category') }}
            </div>

            <div class="form-group">
                <label class="switch switch-default switch-secondary">
                    <input name="active" type="checkbox" class="switch-input" value="1" checked>
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
    </div>
</div>
{{ html()->form()->close() }}

@endsection
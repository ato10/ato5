@extends('layouts.guest')

@section('content')
	<h1>Login</h1>
	<p class="text-muted">Sign In to your account</p>

	<form class="form-horizontal" method="POST" action="{{ route('login') }}">
		{{ csrf_field() }}

		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="icon-user"></i></span>
			</div>
			<input type="text" class="form-control" placeholder="Username" name="email" value="{{ old('email') }}" required autofocus>
			@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>

		<div class="input-group mb-4">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="icon-lock"></i></span>
			</div>
			<input type="password" class="form-control" placeholder="Password" name="password" required>
			@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>

		<div class="row">
			<div class="col-6">
				<button type="submit" class="btn btn-primary px-4">Login</button>
			</div>
			<div class="col-6 text-right">
				<a class="" href="{{ route('password.request') }}">Forgot password?</a>
			</div>
		</div>

	</forn>
@endsection
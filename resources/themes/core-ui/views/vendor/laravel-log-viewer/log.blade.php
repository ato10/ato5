@extends('layouts.app') 

@section('title', 'Logs')

@section('breadcrumb') 
@parent
<li class="breadcrumb-item">Logs</li>
@endsection 

@section('breadcrumb-menu')
@if($current_file)
<li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="?dl={{ base64_encode($current_file) }}">
            <i class="icon-cloud-download"></i> &nbsp;Download file</a>
    </div>
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="?del={{ base64_encode($current_file) }}" id="delete-log">
            <i class="icon-trash"></i> &nbsp;Delete file</a>
    </div>
    @if(count($files) > 1)
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="?delall=true" id="delete-all-log">
            <i class="icon-trash"></i> &nbsp;Delete all files</a>
    </div>
    @endif
</li>
@endif
@endsection

@section('content')
<div class="card">
	<div class="card-header">
		<strong>
			<i class="icon-star"></i> Starcitizen.it</strong>
	</div>
	<div class="card-block">

        @if ($logs === null)
        <div>
            Log file >50M, please download it.
        </div>
        @else
        <div class="table-responsive">
            <table id="table-log" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Level</th>
                        <th>Context</th>
                        <th>Date</th>
                        <th>Content</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($logs as $key => $log)
                    <tr data-display="stack{{{$key}}}">
                        <td class="text-{{{$log['level_class']}}}" style="white-space: nowrap;">
                            <span class="icon-info" aria-hidden="true"></span> &nbsp;{{$log['level']}}</td>
                        <td class="text">{{$log['context']}}</td>
                        <td class="date">{{{$log['date']}}}</td>
                        <td class="text">
                            {{{$log['text']}}} @if (isset($log['in_file']))
                            <br/>{{{$log['in_file']}}}@endif @if ($log['stack'])
                            <div class="stack" id="stack{{{$key}}}" style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                            </div>@endif
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        @endif
    </div>
</div>
@stop

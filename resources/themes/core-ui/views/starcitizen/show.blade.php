@extends('layouts.app')

@section('title', $feed->get_title())

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('starcitizen.index') !!}">Starcitizen.it</a></li>
<li class="breadcrumb-item">{{ $feed->get_title() }}</li>
@endsection

@section('breadcrumb-menu')
<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <a id="btn-zoom" class="btn" href="#" onclick="return zoom(this);"><i class="icon-magnifier-add"></i> &nbsp;Zoom</a>
        <a id="btn-szoom" class="btn" href="#" onclick="return szoom(this);"><i class="icon-magnifier-remove"></i> &nbsp;Zoom</a>
    </div>
</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-star"></i> {{ $feed->get_title() }}</strong></div>
    <div class="card-block">
        <div class="sc-item">
            <small>{{ $feed->get_date() }}</small>
            <p>{!! $feed->get_content() !!}</p>
        </div>
    </div>
</div>
@stop

@section('javascript')
@parent
<script>
function zoom(el) {
    $('#btn-zoom').hide();
    $('#btn-szoom').show();
    $('.sc-item').css('font-size', '1.2rem');
    return false;
}
function szoom(el) {
    $('#btn-szoom').hide();
    $('#btn-zoom').show();
    $('.sc-item').css('font-size', $('body').css('font-size'));
    return false;
}
$(document).ready(function(){
    $('#btn-szoom').hide();
    $('.sc-item img').addClass('img-fluid');
    $('.sc-item iframe').width('100%');
});
</script>
@stop
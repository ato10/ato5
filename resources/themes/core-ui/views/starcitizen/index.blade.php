@extends('layouts.app')

@section('title', 'Starcitizen.it')

@section('breadcrumb')
@parent
<li class="breadcrumb-item">Star Citizen Feeds</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-star"></i> Star Citizen Feeds</strong></div>
    <div class="card-block">
        <div class="sc-items">
            @foreach ($feed->get_items() as $item)
            <div class="sc-item">
                <h4>
                    <a href="{!! route('starcitizen.show', $loop->index) !!}">
                    {{ $item->get_title() }}
                    </a>
                </h4>
                <small>{{ $item->get_date() }}</small>
                <p>{!! $item->get_description() !!}</p>
            </div>
            <hr>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<style type="text/css">
.table-feeds a {
    text-decoration: none;
}
.input-group-btn .btn {
    margin-top: -1px;
}
</style>
@stop

@section('javascript')
@stop

@extends('layouts.app')

@section('title', "Ship matrix")

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('starcitizen.ships') !!}">Starcitizen</a></li>
<li class="breadcrumb-item">Ship matrix</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-star"></i> Ship matrix</strong></div>
    <div class="card-block">
        <div class="ship-matrix">
            @foreach ($ship_matrix->data as $ship)
                <div class="media">
                    @isset ($ship->media)
                    <img class="mr-3 img-rounded" src="{!! route('starcitizen.media', base64_encode($ship->media[0]->source_url)) !!}" alt="{!! $ship->name !!}">
                    @endisset
                    <div class="media-body">
                        <h5 class="mt-0">{!! $ship->name !!}</h5>
                        {!! $ship->description !!}
                        
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@extends('layouts.app')

@section('title', $feed->title)

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('feeds.index') !!}">Feeds</a></li>
<li class="breadcrumb-item">{{ $feed->title }}</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-feed"></i> {{ $feed->title }}</strong></div>
    <div class="card-block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <div class="item">
                            {!! $feed->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<style type="text/css">
.codecolorer-container {
    width: 100% !important;
    margin: 0px !important;
}
.main .panel-body p {
    height: auto !important;
}
</style>
@stop

@section('javascript')
@parent
<script>
//$('.panel-body:first .item:first img').addClass('wow fadeIn');
$(document).ready(function(){
    $('.item img').addClass('img-fluid');
    $('.item iframe').width('100%');
});
</script>
@stop
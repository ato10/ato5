@extends('layouts.app')

@section('title', 'Feed')

@section('breadcrumb')
@parent
<li class="breadcrumb-item">Feeds</li>
@endsection

@section('breadcrumb-menu')
<li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="{!! route('channels.index') !!}">
            <i class="icon-settings"></i> &nbsp;Manage channels</a>
    </div>
</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-feed"></i> Feeds</strong></div>
    <div class="card-block">
                    
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <form id="form-cats" role="search" method="get" action="{!! route('feeds.index') !!}" data-pjax>
                    <select name="c" class="form-control" onchange="$('#form-cats').submit();">
                        <option value="">All categories</option>
                        @foreach ($categories as $category)
                            @if ($category->id == request()->get('c'))
                                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </form>
            </div>
            <div class="col-sm-4 col-md-4">
                <form role="search" method="get" action="{!! route('feeds.index') !!}" data-pjax>
                    <input type="hidden" name="a" value="{{ request('a', '') }}">
                    <input type="hidden" name="c" value="{{ request('c', '') }}">
                    <div class="form-group">
                        <div class="input-group">
                            <input name="q" type="text" class="form-control" id="navbar-search-input"
                                    placeholder="Search" value="{!! Request::get('q', '') !!}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="feed-badges">
                {!! print_filter('q') !!}
                {!! print_filter('c') !!}
                {!! print_filter('a') !!}
                </div>
            </div>
        </div>
        <br>
        @if (count($feeds) > 0)
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped table-hover table-feeds table-responsive">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>&nbsp;</th>
                            <th>Author</th>
                            @if (!request('c', ''))
                            <th>Category</th>
                            @endif
                            <th>Date</th>
                            <th>Size</th>
                            <th width="25">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                    @foreach ($feeds as $feed)
                        <tr class="tr">
                            <td>
                                <div class="d-none d-sm-block">
                                    @if ($feed->content != null)
                                        <a href="{{ route('feeds.show', $feed->slug) }}" data-toggle="tooltip" title="{{ $feed->title }}">{{ $feed->title }}</a>
                                    @else
                                        @if ($feed->source === 'https://ilcorsaronero.info/rss')
                                            <a target="_blank" href="{{ url('/proxy') . '?url=' . $feed->link }}" data-toggle="tooltip" title="{{ $feed->title }}">{{ $feed->title }}</a>
                                        @else
                                            <a target="_blank" href="{{ $feed->link }}" data-toggle="tooltip" title="{{ $feed->title }}">{{ $feed->title }}</a>
                                        @endif
                                    @endif
                                </div>
                                <div class="d-sm-none">
                                    @if ($feed->content != null)
                                        <a href="{{ route('feeds.show', $feed->slug) }}" data-toggle="tooltip" title="{{ $feed->title }}">{{ str_limit($feed->title, 20) }}</a>
                                    @else
                                        <a target="_blank" href="{{ $feed->link }}" data-toggle="tooltip" title="{{ $feed->title }}">{{ str_limit($feed->title, 20) }}</a>
                                    @endif
                                </div>
                            </td>
                            <td>
                            @if ($feed->category->id == 4 || $feed->category->id == 29 || $feed->category->id == 11)
                            <a class="btn btn-warning btn-sm" href="{{ route('tmdb.show', $feed) }}"><span class="icon-magnifier"></a></a>
                            @endif
                            </td>
                            <td><a href="{!! get_filter('a', $feed->author) !!}">{{ $feed->author }}</a></td>
                            @if (!request('c', ''))
                            <td><a data-pjax href="{!! get_filter('c', $feed->category->id) !!}">{{ $feed->category->name }}</a></td>
                            @endif
                            <td>{{ $feed->pubDate->format('d/m/Y H:i') }}</td>
                            <td class="text-right nowrap">
                                @if ($feed->enclosure_length)
                                        {!! human_filesize($feed->enclosure_length) !!}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @if ($feed->enclosure_type === 'application/x-bittorrent')
                                    <a href="{!! $feed->enclosure_url !!}" target="_blank">
                                        <i class="icon icon-cloud-download" aria-hidden="true"></i>
                                    </a>
                                @else
                                    @if ($feed->source === 'https://ilcorsaronero.info/rss')
                                        <a href="{!! url('/proxy') . '?url=' . $feed->link !!}" target="_blank"><i class="icon icon-link" aria-hidden="true"></i></a>
                                    @else
                                        <a href="{!! $feed->link !!}" target="_blank"><i class="icon icon-link" aria-hidden="true"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            {!! $feeds->links() !!}
        </div>
        @else
        <div class="row">
            <div class="col-sm-12">
                <p>No records found.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <form target="_blank" action="http://www.btloft.com/search" method="get">
                    <div class="form-group">
                        <label for="query">Btloft.com</label>
                        <input class="form-control" id="query" name="query" value="{!! Request::get('q', '') !!}" type="text">
                    </div>
                    <input class="btn btn-default" type="submit" value="cerca" alt="Submit">
                </form>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@section('css')
@parent
<style type="text/css">
.table-feeds a {
    text-decoration: none;
}
.input-group-btn .btn {
    margin-top: -1px;
}
</style>
@stop

@section('javascript')
@parent
<script src="{!! theme_url('plugins/infinite-scroll/dist/infinite-scroll.pkgd.min.js') !!}"></script>
<script>
$('.table-feeds .tbody').infiniteScroll({
    path: '.pagination a[rel=next]',
    append: '.tr',
    history: false,
});
</script>
@stop

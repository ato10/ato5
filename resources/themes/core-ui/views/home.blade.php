@extends('layouts.app') @section('breadcrumb')
<li class="breadcrumb-item">Home</li>
@endsection @section('content')

<div class="row">
	<div class="col">
		@widget('serieWidget')
	</div>
	<!--/.col-->

	<div class="col">
		@widget('filmWidget')
	</div>
	<!--/.col-->

	<div class="col">
		@widget('gameWidget')
	</div>
	<!--/.col-->

</div>
{{--
<div class="card">
	<div class="card-header">Dashboard</div>
	<div class="card-block">
		@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
		@endif You are logged in!
	</div>
</div>
--}}
@endsection
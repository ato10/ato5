@section('sidebar-nav')
<li class="nav-item">
    <a class="nav-link{!! classActivePath('home') !!}" href="{!! route('home') !!}">
        <i class="icon-speedometer"></i> Dashboard
    </a>
</li>
<li class="nav-item">
    <a class="nav-link{!! classActiveSegment(1, 'feeds') !!}" href="{!! route('feeds.index') !!}">
        <i class="icon-feed"></i> Feeds
    </a>
</li>
<li class="nav-item">
    <a class="nav-link{!! classActiveSegment(1, 'starcitizen') !!}" href="{!! route('starcitizen.index') !!}">
        <i class="icon-star"></i> Star Citizen Feeds
    </a>
</li>
<li class="nav-item">
    <a class="nav-link{!! classActiveSegment(1, 'mysqladmin') !!}" href="{!! route('mysqladmin') !!}">
        <i class="icon-compass"></i> Mysqladmin
    </a>
</li>
<li class="nav-item">
    <a class="nav-link{!! classActivePath('backup') !!}" href="{!! route('backup.index') !!}">
        <i class="icon-drawer"></i> Backups
    </a>
</li>
<li class="nav-item">
    <a class="nav-link{!! classActivePath('logs') !!}" href="{!! route('logs') !!}">
        <i class="icon-list"></i> Logs
    </a>
</li>
@show
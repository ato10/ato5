@extends('layouts.app')

@section('title', 'Mysqladmin')

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('mysqladmin') !!}">Mysqladmin</a></li>
<li class="breadcrumb-item">{{ env('DB_DATABASE') }}</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-compass"></i> Mysqladmin</strong></div>
    <div class="card-block">

        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <tr>
                    <th>Name</th>
                    <th>Engine</th>
                    <th>Rows</th>
                    <th>Size</th>
                    <th>Collation</th>
                    <th>Created</th>
                </tr>
                @foreach ($tables as $table)
                <tr>
                    <td>
                        <a href="{!! route('mysqladmin.select', $table->Name) !!}">
                            {!! $table->Name !!}
                        </a>
                    </td>
                    <td>{!! $table->Engine !!}</td>
                    <td class="text-right">{!! $table->Rows !!}</td>
                    <td class="text-right">{!! human_filesize($table->Data_length) !!}</td>
                    <td>{!! $table->Collation !!}</td>
                    <td>{!! $table->Create_time !!}</td>
                </tr>
                @endforeach
            </table>
        </div>

    </div>
</div>
@endsection
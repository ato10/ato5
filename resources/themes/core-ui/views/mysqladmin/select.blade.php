@extends('layouts.app')

@section('title', 'Mysqladmin')

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('mysqladmin') !!}">Mysqladmin</a></li>
<li class="breadcrumb-item"><a href="{!! route('mysqladmin') !!}">{{ env('DB_DATABASE') }}</a></li>
<li class="breadcrumb-item">Query</li>
@endsection

@section('breadcrumb-menu')
<li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="{!! route('mysqladmin.table', $table) !!}">
            <i class="icon-settings"></i> &nbsp;Structure</a>
    </div>
</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-compass"></i> Mysqladmin</strong></div>
    <div class="card-block">

        <form method="post" action="{!! route('mysqladmin.select', $table) !!}">
            {{ csrf_field() }}
            <div class="form-group">
                <textarea class="form-control" name="sql" id="sql">{!! $sql !!}</textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-sm">
                <span class="icon-energy"></span> Execute
            </button>
        </form>
        <br><br>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                @foreach ($result as $rows)
                    @if ($loop->first)
                    <tr>
                        @foreach ($rows as $key => $value)
                            <th>{{ $key }}</th>
                        @endforeach
                    </tr>
                    @endif
                    <tr>
                        @foreach ($rows as $key => $value)
                        <td>{{ str_limit($value, 50) }}</td>
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
</div>
@endsection

@section('css')
@parent
<link href="{!! theme_url('plugins/codemirror/lib/codemirror.css') !!}" rel="stylesheet">
<link rel="stylesheet" href="{!! theme_url('plugins/codemirror/addon/hint/show-hint.css') !!}" />
<style>
.CodeMirror {
    height: 100px;
}
</style>
@endsection

@section('javascript')
@parent
<script src="{!! theme_url('plugins/codemirror/lib/codemirror.js') !!}"></script>
<script src="{!! theme_url('plugins/codemirror/mode/sql/sql.js') !!}"></script>
<script src="{!! theme_url('plugins/codemirror/addon/hint/show-hint.js') !!}"></script>
<script src="{!! theme_url('plugins/codemirror/addon/hint/sql-hint.js') !!}"></script>
<script>
var editor = CodeMirror.fromTextArea(document.getElementById('sql'), {
    mode: 'text/x-mysql',
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: true,
    extraKeys: {"Ctrl-Space": "autocomplete"}
});
</script>
@endsection
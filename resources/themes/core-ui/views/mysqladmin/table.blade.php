@extends('layouts.app')

@section('title', 'Mysqladmin - ' . $table)

@section('breadcrumb')
@parent
<li class="breadcrumb-item"><a href="{!! route('mysqladmin') !!}">Mysqladmin</a></li>
<li class="breadcrumb-item"><a href="{!! route('mysqladmin') !!}">{{ env('DB_DATABASE') }}</a></li>
<li class="breadcrumb-item">{{ $table }}</li>
@endsection

@section('breadcrumb-menu')
<li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="{!! route('mysqladmin.select', $table) !!}">
            <i class="icon-list"></i> &nbsp;Data</a>
    </div>
</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header"><strong><i class="icon-compass"></i> Mysqladmin</strong></div>
    <div class="card-block">

        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <tr>
                    <th>Field</th>
                    <th>Type</th>
                    <th>Null</th>
                    <th>Key</th>
                    <th>Default</th>
                    <th>Extra</th>
                </tr>
                @foreach ($columns as $column)
                <tr>
                    <td>{!! $column->Field !!}</td>
                    <td>{!! $column->Type !!}</td>
                    <td>{!! $column->Null !!}</td>
                    <td>{!! $column->Key !!}</td>
                    <td>{!! $column->Default !!}</td>
                    <td>{!! $column->Extra !!}</td>
                </tr>
                @endforeach
            </table>
        </div>

    </div>
</div>
@endsection
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="CoreUI Bootstrap 4 Admin Template">
	<meta name="author" content="Lukasz Holeczek">
	<meta name="keyword" content="CoreUI Bootstrap 4 Admin Template">
	<!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
	<title>{{ config('site.title') }}</title>
	
	<!-- Icons -->
	<link href="{!! theme_url('plugins/font-awesome/css/fontawesome-all.min.css') !!}" rel="stylesheet">
	<link href="{!! theme_url('plugins/simple-line-icons/css/simple-line-icons.css') !!}" rel="stylesheet">
	
	<!-- Main styles for this application -->
	<link href="{!! theme_url('css/style.min.css') !!}" rel="stylesheet">
	<!-- Styles required by this views -->

</head>

<body class="app flex-row align-items-center">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card-group">
					<div class="card p-4">
						<div class="card-body">
							@yield('content')
						</div>
					</div>
					<!--<div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
						<div class="card-body text-center">
							<div>
								<h2>Sign up</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
								<button type="button" class="btn btn-primary active mt-3">Register Now!</button>
							</div>
						</div>
					</div>-->
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap and necessary plugins -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
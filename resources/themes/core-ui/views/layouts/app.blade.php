<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
	<meta name="author" content="Łukasz Holeczek">
	<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
	{{--<link rel="shortcut icon" href="{!! theme_url('img/favicon.png') !!}">--}}
	
	<title>{{ config('site.title') }} | @yield('title')</title>

	<!-- Icons -->
	<link href="{!! theme_url('plugins/font-awesome/css/fontawesome-all.min.css') !!}" rel="stylesheet">
	<link href="{!! theme_url('plugins/simple-line-icons/css/simple-line-icons.css') !!}" rel="stylesheet">

	<link rel="apple-touch-icon" sizes="57x57" href="{!! theme_url('icons/apple-icon-57x57.png') !!}">
	<link rel="apple-touch-icon" sizes="60x60" href="{!! theme_url('icons/apple-icon-60x60.png') !!}">
	<link rel="apple-touch-icon" sizes="72x72" href="{!! theme_url('icons/apple-icon-72x72.png') !!}">
	<link rel="apple-touch-icon" sizes="76x76" href="{!! theme_url('icons/apple-icon-76x76.png') !!}">
	<link rel="apple-touch-icon" sizes="114x114" href="{!! theme_url('icons/apple-icon-114x114.png') !!}">
	<link rel="apple-touch-icon" sizes="120x120" href="{!! theme_url('icons/apple-icon-120x120.png') !!}">
	<link rel="apple-touch-icon" sizes="144x144" href="{!! theme_url('icons/apple-icon-144x144.png') !!}">
	<link rel="apple-touch-icon" sizes="152x152" href="{!! theme_url('icons/apple-icon-152x152.png') !!}">
	<link rel="apple-touch-icon" sizes="180x180" href="{!! theme_url('icons/apple-icon-180x180.png') !!}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{!! theme_url('icons/android-icon-192x192.png') !!}">
	<link rel="icon" type="image/png" sizes="32x32" href="{!! theme_url('icons/favicon-32x32.png') !!}">
	<link rel="icon" type="image/png" sizes="96x96" href="{!! theme_url('icons/favicon-96x96.png') !!}">
	<link rel="icon" type="image/png" sizes="16x16" href="{!! theme_url('icons/favicon-16x16.png') !!}">

	<!-- Main styles for this application -->
	<link href="{!! theme_url('css/style.min.css') !!}" rel="stylesheet">
	<!-- Styles required by this views -->
	@section('css')
	@show

</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Brand options
1. '.brand-minimized'       - Minimized brand (Only symbol)

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-minimized'			- Minimized Sidebar (Only icons)
5. '.sidebar-compact'			  - Compact Sidebar

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Breadcrumb options
1. '.breadcrumb-fixed'			- Fixed Breadcrumb

// Footer options
1. '.footer-fixed'					- Fixed footer

-->

<body class="app
    {!! config('theme.header-fixed') ? ' header-fixed' : '' !!}
    {!! config('theme.brand-minimized') ? ' brand-minimized' : '' !!}
    {!! config('theme.sidebar-fixed') ? ' sidebar-fixed' : '' !!}
    {!! config('theme.sidebar-hidden') ? ' sidebar-hidden' : '' !!}
    {!! config('theme.sidebar-off-canvas') ? ' sidebar-off-canvas' : '' !!}
    {!! config('theme.sidebar-minimized') ? ' sidebar-minimized' : '' !!}
    {!! config('theme.sidebar-compact') ? ' sidebar-compact' : '' !!}
    {!! config('theme.aside-menu-fixed') ? ' aside-menu-fixed' : '' !!}
    {!! config('theme.aside-menu-hidden') ? ' aside-menu-hidden' : '' !!}
    {!! config('theme.aside-menu-off-canvas') ? ' aside-aside-menu-off-canvas' : '' !!}
    {!! config('theme.breadcrumb-fixed') ? ' breadcrumb-fixed' : '' !!}
    {!! config('theme.footer-fixed') ? ' footer-fixed' : '' !!}
    ">
	<header class="app-header navbar">
		<button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="{!! route('home') !!}"></a>
		<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
			<span class="navbar-toggler-icon"></span>
		</button>

		
        @include('partials.menu_topleft')
        @include('partials.menu_topright')
        @if (config('theme.aside-menu-toggler'))
		<button class="navbar-toggler aside-menu-toggler" type="button">
			<span class="navbar-toggler-icon"></span>
		</button>
        @endif

	</header>

	<div class="app-body">
		<div class="sidebar">
			<nav class="sidebar-nav">
				<ul class="nav">
					@include('partials.sidebar_nav')
				</ul>
			</nav>
			<button class="sidebar-minimizer brand-minimizer" type="button"></button>
		</div>

		<!-- Main content -->
		<main class="main">

			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				@section ('breadcrumb')
				<li class="breadcrumb-item"><a href="{!! route('home') !!}">Home</a></li>
				@show

				<!-- Breadcrumb Menu-->
				@section('breadcrumb-menu')
				{{--<li class="breadcrumb-menu d-md-down-none">
					<div class="btn-group" role="group" aria-label="Button group">
						<a class="btn" href="#">
							<i class="icon-speech"></i>
						</a>
						<a class="btn" href="./">
							<i class="icon-graph"></i> &nbsp;Dashboard</a>
						<a class="btn" href="#">
							<i class="icon-settings"></i> &nbsp;Settings</a>
					</div>
				</li>--}}
				@show
			</ol>

			<div class="container-fluid">
				@yield('content')
			</div>
			<!-- /.conainer-fluid -->
		</main>

        @if (config('theme.aside-menu'))
            @include('partials.aside_menu')
        @endif

	</div>

	<footer class="app-footer">
		<span>
			<a href="http://coreui.io">CoreUI</a> © 2017 creativeLabs.</span>
		<span class="ml-auto">Powered by
			<a href="http://coreui.io">CoreUI</a>
		</span>
	</footer>

	<!-- Bootstrap and necessary plugins -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="{!! theme_url('plugins/pace-progress/pace.min.js') !!}"></script>

	<!-- CoreUI main scripts -->

	<script src="{!! theme_url('js/app.js') !!}"></script>

	@section('javascript')
	<script>
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>
	@show

</body>

</html>
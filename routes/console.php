<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('test:mail', function () {
    $title = "Test email";
    $content = "Testo della mail";
    Mail::send('emails.test', ['title' => $title, 'content' => $content], function ($message)
    {
        $message->from('al.biasi@gmail.com', 'Alessandro Biasi');
        $message->to('al.biasi@gmail.com');
        $message->subject('Test email');
    });
})->describe('Invia una mail di prova');

Artisan::command('icnupdate', function () {
    $feeds = \App\Models\Feed::where('source', 'like', '%ilcorsaronero.info/rss')
            ->where('category_id', 25)->get();
    foreach ($feeds as $feed) {
        if (str_contains($feed->title, '[Musica]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|2');
            $feed->category_id = 2;
        } else if (str_contains($feed->title, '[Serie TV]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|29');
            $feed->category_id = 29;
        } else if (str_contains($feed->title, '[Apps Win]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|10');
            $feed->category_id = 10;
        } else if (str_contains($feed->title, '[Altro]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|25');
            $feed->category_id = 25;
        } else if (str_contains($feed->title, '[Anime]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|7');
            $feed->category_id = 7;
        } else if (str_contains($feed->title, '[PC Games]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|11');
            $feed->category_id = 11;
        } else if (str_contains($feed->title, '[Apps Mac]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|9');
            $feed->category_id = 9;
        } else if (str_contains($feed->title, '[Ebook') || str_contains($feed->title, '[E-Books]')) {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|3');
            $feed->category_id = 3;
        } else {
            $this->info('---'.$feed->title.'|'.$feed->category_id.'|4');
            $feed->category_id = 4;
        }
        $feed->save();
    }
    \Cache::flush();
})->describe('Display an inspiring quote');
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('admin', function() {
        return redirect('/');
    });

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('feeds', 'FeedController@index')->name('feeds.index');
    Route::get('feeds/{slug}', 'FeedController@show')->name('feeds.show');

    Route::resource('channels', 'ChannelController');

    Route::get('starcitizen', 'StarcitizenController@index')->name('starcitizen.index');
    Route::get('starcitizen/ships', 'StarcitizenController@ships')->name('starcitizen.ships');
    Route::get('starcitizen/media/{url}', function($url) {
        $img = Image::cache(function($image) use ($url) {
            $image->make("https://robertsspaceindustries.com" . base64_decode($url))->resize(300, 90);
        }, 10, true);
        return $img->response('jpg');
    })->name('starcitizen.media');
    Route::get('starcitizen/{slug}', 'StarcitizenController@show')->name('starcitizen.show');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

    Route::get('mysqladmin', 'MysqladminController@index')->name('mysqladmin');
    Route::get('mysqladmin/table/{table}', 'MysqladminController@table')->name('mysqladmin.table');
    Route::match(['get', 'post'], 'mysqladmin/select/{table}', 'MysqladminController@select')->name('mysqladmin.select');

    Route::get('tmdb/{feed}', 'TmdbController@movie')->name('tmdb.show');

    Route::get('backup', 'BackupController@index')->name('backup.index');

    Route::get('proxy', function () {
        $url = request('url');
        $arrContextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];
        $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $content = str_ireplace('//s.ilcorsaronero.info/css/main.css', '/css/icn.css', $content);
        $content = str_ireplace('src="//s.ilcorsaronero.info', 'src="/proxy?url=https://s.ilcorsaronero.info', $content);
        $content = str_ireplace('src=\'//s.ilcorsaronero.info', 'src=\'/proxy?url=https://s.ilcorsaronero.info', $content);
        $content = str_ireplace('href="//s.ilcorsaronero.info', 'href="/proxy?url=https://s.ilcorsaronero.info', $content);
        $content = str_ireplace('href=\'//s.ilcorsaronero.info', 'href=\'/proxy?url=https://s.ilcorsaronero.info', $content);
        $content = str_ireplace('href="/tor/', 'href="/proxy?url=https://ilcorsaronero.info/tor/', $content);
        $content = str_ireplace('href=\'/tor/', 'href=\'/proxy?url=https://ilcorsaronero.info/tor/', $content);
        $content = str_ireplace('href=/tor/', 'href=/proxy?url=https://ilcorsaronero.info/tor/', $content);
        $content = str_ireplace('action="/tor/', 'action="/proxy?url=https://ilcorsaronero.info/tor/', $content);
        $content = str_ireplace('action=\'/tor/', 'action=\'/proxy?url=https://ilcorsaronero.info/tor/', $content);
        $content = str_ireplace('action=/tor/', 'action=/proxy?url=https://ilcorsaronero.info/tor/', $content);
        echo $content;
    });

});

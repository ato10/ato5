<?php

namespace App\Console;


use Illuminate\Console\Command;
use App\Models\Feed;
use App\Models\Channel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Update extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update feeds.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channels = Channel::active()->get();
        foreach ($channels as $channel) {
            if ($channel->type === 'rss20') {
                $this->rss20($channel);
            } else if ($channel->type === 'rss20_2') {
                $this->rss20_2($channel);
            }
        }
        \Cache::flush();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    private function rss20(Channel $channel)
    {
        $this->info('---');
        $this->info($channel->url);

        try {

        libxml_use_internal_errors(true);
        $opts = [
            'http' => [
                'timeout' => 5
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];
        $context  = stream_context_create($opts);
        $feed = trim(file_get_contents($channel->url, false, $context));
        //$feed = trim(file_get_contents($channel->url));
        //$feed = trim($res->getBody());
        $rss = simplexml_load_string($feed);

        if ($rss !== false) {
            foreach ($rss->channel->item as $item) {
                $f = Feed::where('guid', $item->guid)->first();
                if ($f === null) {
                    $f = new Feed;
                }
                if ($item->enclosure && $item->enclosure->attributes()) {
                    foreach ($item->enclosure->attributes() as $key => $value) {
                        switch (trim((string)$key)) {
                            case 'url':
                                $f->enclosure_url = trim((string)$value);
                            break;
                            case 'length':
                                $f->enclosure_length = trim((string)$value);
                            break;
                            case 'type':
                                $f->enclosure_type = trim((string)$value);
                            break;
                        }
                    }
                }
                $f->guid = trim((string)$item->guid);
                $f->guid_permalink = false;
                if ($item->guid && $item->guid->attributes()) {
                    foreach ($item->guid->attributes() as $key => $value) {
                        if (trim((string)$key) === 'isPermaLink' && trim((string)$value) === 'true') {
                            $f->guid_permalink = true;
                        }
                    }
                }
                $f->source = $channel->url;
                $f->title = trim((string)$item->title);
                $f->slug = str_slug(trim((string)$item->title), '-');
                $f->link = trim((string)$item->link);
                $f->description = trim((string)$item->description);
                $f->content = null;
                //$f->pubDate = trim((string)$item->pubDate);
                $f->pubDate = new \Carbon\Carbon(trim((string)$item->pubDate));
                $f->comments = trim((string)$item->comments);
                if ($channel->category === 25) {
                    $f->category_id = $this->get_cat_from_title($item);
                } else if ($channel->category === null) {
                    if ($item->category) {
                        $f->category_id = trim((string)$item->category);
                    } else {
                        $f->category_id = 25;
                    }
                } else {
                    $f->category_id = $channel->category;
                }
                $f->author = trim((string)$item->author);
                $f->save();
            }
        } else {
            foreach (libxml_get_errors() as $error) {
                $this->error($error->message);
                break;
            }
            libxml_clear_errors();
        }

        } catch (\Exception $e) {
            dump ($e);
            /*$title = "Feed problem";
            $content = $channel;
            \Mail::send('emails.test', ['title' => $title, 'content' => $content], function ($message)
            {
                $message->from('al.biasi@gmail.com', 'Alessandro Biasi');
                $message->to('al.biasi@gmail.com');
                $message->subject('Feed problem');
            });*/
        }
    }

    private function rss20_2(Channel $channel)
    {
        $this->info('---');
        $this->info($channel->url);

        libxml_use_internal_errors(true);
        $feed = trim(file_get_contents($channel->url));
        $rss = simplexml_load_string($feed);

        if ($rss !== false) {
            foreach ($rss->channel->item as $item) {
                $dc = $item->children("http://purl.org/dc/elements/1.1/");
                $content = $item->children("content", true);
                $f = Feed::where('guid', $item->guid)->first();
                if ($f === null) {
                    $f = new Feed;
                }
                if ($item->enclosure && $item->enclosure->attributes()) {
                    foreach ($item->enclosure->attributes() as $key => $value) {
                        switch (trim((string)$key)) {
                            case 'url':
                                $f->enclosure_url = trim((string)$value);
                            break;
                            case 'length':
                                $f->enclosure_length = trim((string)$value);
                            break;
                            case 'type':
                                $f->enclosure_type = trim((string)$value);
                            break;
                        }
                    }
                }
                $f->guid = trim((string)$item->guid);
                $f->guid_permalink = false;
                if ($item->guid && $item->guid->attributes()) {
                    foreach ($item->guid->attributes() as $key => $value) {
                        if (trim((string)$key) === 'isPermaLink' && trim((string)$value) === 'true') {
                            $f->guid_permalink = true;
                        }
                    }
                }
                $f->source = $channel->url;
                $f->title = trim((string)$item->title);
                $f->slug = str_slug(trim((string)$item->title), '-');
                $f->link = trim((string)$item->link);
                $f->description = trim((string)$item->description);
                if ($content) {
                    $f->content = trim((string)$content->encoded);
                }

                //$f->pubDate = trim((string)$item->pubDate);
                $f->pubDate = new \Carbon\Carbon(trim((string)$item->pubDate));
                $f->comments = trim((string)$item->comments);
                if ($channel->category === 25) {
                    $f->category_id = $this->get_cat_from_title($item);
                } else if ($channel->category === null) {
                    if ($item->category) {
                        $f->category_id = trim((string)$item->category);
                    } else {
                        $f->category_id = 25;
                    }
                } else {
                    $f->category_id = $channel->category;
                }
                $f->author = trim((string)$dc->creator);
                $f->save();
            }
        } else {
            foreach (libxml_get_errors() as $error) {
                $this->error($error->message);
                break;
            }
            libxml_clear_errors();
        }
    }

    private function get_cat_from_title($item)
    {
        if (str_contains((string)$item->title, '[Musica]')) {
            return 2;
        } else if (str_contains((string)$item->title, '[Serie TV]')) {
            return 29;
        } else if (str_contains((string)$item->title, '[Apps Win]')) {
            return 10;
        } else if (str_contains((string)$item->title, '[Altro]')) {
            return 25;
        } else if (str_contains((string)$item->title, '[Anime]')) {
            return 7;
        } else if (str_contains((string)$item->title, '[PC Games]')) {
            return 11;
        } else if (str_contains((string)$item->title, '[Ebook') || str_contains($item->title, '[E-Books]')) {
            return 3;
        } else if (str_contains((string)$item->title, '[Apps Mac]')) {
            return 9;
        } else {
            //if (str_contains((string)$item->source, 'ilcorsaronero')) {
            //    return 4;
            //} else {
                return 25;
            //}
        }
    }
}

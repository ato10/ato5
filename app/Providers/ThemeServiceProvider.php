<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datetime', function ($expression) {
            return "<?php echo ($expression)->format('m/d/Y H:i'); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $theme = config('site.theme');
        $theme_path = base_path() . '/resources/themes/' . $theme;
        \Config::set('view.paths', array_merge(
            [$theme_path . '/views/'], 
            \Config::get('view.paths')
        ));
        \View::setFinder($this->app['view.finder']);
        if (\File::exists($theme_path . '/theme.json')) {
            $json = \File::get($theme_path . '/theme.json');
            \Config::set('theme', (array)json_decode($json));
        }
    }
}
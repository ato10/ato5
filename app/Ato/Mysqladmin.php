<?php

namespace App\Ato;

class Mysqladmin
{

    public function getTables()
    {
        $tables = \DB::select('show table status');
        return $tables;
    }

    public function getColumns($table)
    {
        //$columns = \DB::select('show columns from ?', [$table]);
        $columns = \DB::select('show columns from ' . $table);
        return $columns;
    }

    public function getQuery($table, $sql)
    {
        if ($sql == null) {
            $result = \DB::select($sql);
        } else {
            $result = \DB::select($sql);
        }
        return $result;
    }

}
<?php

namespace App\Models;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use Rememberable;

    protected $fillable = [];
    protected $dates = [ 'pubDate' ];

    public function category()
    {
        return $this->belongsTo('App\Models\FeedCategory');
    }
}

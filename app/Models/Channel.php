<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = ['name', 'url', 'type', 'category', 'active'];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}

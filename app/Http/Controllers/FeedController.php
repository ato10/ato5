<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use App\Models\FeedCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q', '');
        $a = $request->get('a', '');
        $c = $request->get('c', null);
        //\Cache::flush();
        $feeds = Feed::remember(5)
                //->with('category')
                ->with(['category' => function ($query) { $query->remember(5); }])
                ->where('title', 'like', "%$q%")
                ->where('author', 'like', "%$a%")
                ->whereHas('category', function ($query) use ($c) {
                    if ($c !== null || !empty($c)) {
                        $query->remember(5)->where('id', $c);
                    }
                })
                ->orderBy('pubDate', 'desc')->paginate(30);
        $feeds->setPath($request->fullUrlWithQuery(['page' => null]));
        $categories = FeedCategory::remember(5)->orderby('name', 'asc')->get();
        return view('feeds.index')->with(compact('feeds'))->with(compact('categories'));
    }

        /**
     * Show the specified resource.
     * @return Response
     */
    public function show($slug)
    {
        $feed = Feed::where('slug', $slug)->first();
        if ($feed == null) {
            \App::abort(404);
        }
        return view('feeds.show')->with(compact('feed'));
    }

    public function ebook()
    {
        $string = "Clara Sánchez - La voce invisibile del vento [Pdf Epub Azw3 - Ita]";
        $author = strstr($string, ' - ', true);
        $tmp = str_replace(' - ', '', strstr($string, ' - '));
        $title = strstr($tmp, ' [', true);
        return $title;
    }
}

<?php

namespace App\Http\Controllers;

use Facades\App\Ato\Mysqladmin;
use Illuminate\Http\Request;

class MysqladminController extends Controller
{

    public function index()
    {
        $tables = Mysqladmin::getTables();
        //dump($tables);
        return view('mysqladmin.tables')->with(compact('tables'));
    }

    public function table($table)
    {
        $columns = Mysqladmin::getColumns($table);
        //dump($columns);
        return view('mysqladmin.table')
                ->with(compact('columns'))
                ->with(compact('table'));
    }

    public function select(Request $request, $table)
    {
        $sql = $request->get('sql', "SELECT * FROM `$table` LIMIT 25");
        $result = Mysqladmin::getQuery($table, $sql);
        //dump($result);
        return view('mysqladmin.select')
                ->with(compact('result'))
                ->with(compact('table'))
                ->with(compact('sql'));
    }

}

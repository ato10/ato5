<?php

namespace App\Http\Controllers;

use \Storage;
use Illuminate\Http\Request;
use \Spatie\Backup\Tasks\Monitor\BackupDestinationStatus;
use \Spatie\Backup\Tasks\Monitor\BackupDestinationStatusFactory;

class BackupController extends Controller
{

    function index()
    {
        
        $backups = collect();
        $files = Storage::disk('backup')->files('/Ato');
        foreach ($files as $file) {
            $lm = new \Carbon\Carbon;
            $backups->push([
                'filename' => $file,
                'basename' => basename($file),
                'size' => Storage::disk('backup')->size($file),
                'modified' => $lm->createFromTimestamp(Storage::disk('backup')->lastModified($file)),
            ]);
        }
        //dd($backups);
        return view('backup.index')
                ->with(compact('backups'));
    }

}

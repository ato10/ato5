<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use App\Models\FeedCategory;
use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;

class TmdbController extends Controller
{
    public function movie(Feed $feed)
    {
        if ($feed->category->id == 4) {
            $sr = ['4K', 'HDR', 'UHD'];
            $search = trim(str_replace($sr, '', strstr($feed->title, '(', true)));
            $year = substr(strstr(strstr($feed->title, ')', true), '('), 1);
            $tmdb = Tmdb::getSearchApi()->searchMovies($search);
            if (empty($tmdb['results'])) {
                if (strstr($search, '-', true)) {
                    $tmdb = Tmdb::getSearchApi()->searchMovies(trim(strstr($search, '-', true)));
                }
            }
            foreach ($tmdb['results'] as $movie) {
                if (strpos($movie['release_date'], $year) !== false) {
                    $tmdb['results'] = [ $movie ];
                    break;
                }
            }
            return view('tmdb.movie')
                    ->with(compact('search'))
                    ->with(compact('feed'))
                    ->with(compact('tmdb'));
        } else if ($feed->category->id == 29) {
            $search = trim(strstr($feed->title, 'S0', true));
            if (empty($search)) {
                $search = trim(strstr($feed->title, 'S1', true));
            }
            if (empty($search)) {
                $search = trim(strstr($feed->title, 'S2', true));
            }
            $tmdb = Tmdb::getSearchApi()->searchTv($search);
            if (empty($tmdb['results'])) {
                if (strstr($search, '-', true)) {
                    $tmdb = Tmdb::getSearchApi()->searchTv(trim(strstr($search, '-', true)));
                }
            }
            foreach ($tmdb['results'] as $movie) {
                if (strtolower($movie['name']) === strtolower($search)) {
                    $tmdb['results'] = [ $movie ];
                    break;
                }
            }
            return view('tmdb.tv')
                    ->with(compact('search'))
                    ->with(compact('feed'))
                    ->with(compact('tmdb'));
        } else if ($feed->category->id == 11) {
            $games = \IGDB::searchGames($feed->title);
            if (!empty($games)) {
                $games = [$games[0]];
            }
            //dump($games);
            return view('tmdb.game')
                    ->with(compact('search'))
                    ->with(compact('feed'))
                    ->with(compact('games'));
        }
        
    }
}

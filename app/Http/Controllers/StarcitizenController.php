<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;

class StarcitizenController extends Controller
{
    private $feed;
    public function __construct()
    {
        $this->feed = new \SimplePie();
        $this->feed->set_cache_location(storage_path('framework/cache/'));
        $this->feed->set_feed_url([
            "https://www.starcitizenitalia.com/feed/",
            "https://starcitizen.it/feed/"
        ]);
        $this->feed->init();
        $this->feed->handle_content_type();
        //clock($this->feed);
    }

    public function index()
    {
        return view('starcitizen.index', [
            'feed'   => $this->feed
        ]);
    }

    public function show($key)
    {
        return view('starcitizen.show', [
            'feed'   => $this->feed->get_item($key),
            'image' => $this->feed->get_image_url(),
        ]);
    }

    public function ships()
    {
        if (!Storage::exists('ship-matrix.json')) {
            $json = file_get_contents('https://robertsspaceindustries.com/ship-matrix/index');
            Storage::put('ship-matrix.json', $json);
        } else {
            $json = Storage::get('ship-matrix.json');
        }
        $ship_matrix = json_decode($json);
        //dump($ship_matrix->data[0]);
        return view('starcitizen.ships')
                ->with(compact('ship_matrix'));
    }
}

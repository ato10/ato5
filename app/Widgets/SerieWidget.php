<?php

namespace App\Widgets;

use App\Models\FeedCategory;
use Arrilot\Widgets\AbstractWidget;

class SerieWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'title' => 'Serie TV',
        'cat_id' => 29
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \DB::table('feeds')->where('category_id', $this->config['cat_id'])->count();
        $cat = FeedCategory::findOrFail($this->config['cat_id']);
        $string = $cat->name;

        return view('widgets.feed_widget', [
            'config' => $this->config,
        ])->with([
            'title' => "{$count} {$string}",
            'icon' => 'icon-film',
            'link' => url('feeds?c=' . $this->config['cat_id'])
        ]);
    }
}
